---
date: '2020-01-10'
title: 'Java Developer'
company: 'Kundoluk'
location: 'Bishkek, KG'
range: 'Jan 2020 - Dec 2021'
url: 'https://kundoluk.kg/'
---

- Utilized MapStruct for seamless mapping between data objects and DTOs, improving code readability and reducing development time.
- Implemented Liquibase for database schema versioning and management, enabling smooth database migrations and reducing deployment issues.
- Leveraged Elasticsearch for efficient indexing and search capabilities, improving performance and enabling quick retrieval of large datasets.
- Developed and integrated a messaging system using Apache Kafka, enabling asynchronous communication between different components of the application and ensuring scalability.
