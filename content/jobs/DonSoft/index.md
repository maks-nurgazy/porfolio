---
date: '2019-07-01'
title: 'Software Engineer Co-op'
company: 'DonSoft'
location: 'Bishkek, KG'
range: 'Jul 2019 - Dec 2019'
url: 'https://donsoft.kg/'
---

- Collaborated with cross-functional teams to design and implement scalable and efficient solutions, ensuring high-quality code and adherence to best practices.
- Assisted in the design and development of a microservice architecture, enhancing the scalability and maintainability of the application.
- Actively participated in Agile development processes, including daily stand-up meetings, sprint planning, and retrospective meetings, ensuring efficient project management and timely delivery.
