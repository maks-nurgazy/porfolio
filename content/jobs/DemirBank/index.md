---
date: '2024-04-01'
title: 'Java Backend Developer'
company: 'Demir Bank'
location: 'Bishkek, KG'
range: 'Jul 2023 - Apr 2024'
url: 'https://demirbank.kg/en'
---

- Developed and maintained an online loan application from scratch, utilizing Java backend applications with Quarkus.
- Optimized PL/SQL queries to improve database performance.
- Integrated rule engines like Kogito, BPMN, and Drools to streamline decision-making processes and enhance application functionality.
