---
date: '2021-05-19'
title: 'Node.js Developer'
company: 'Mancho devs'
location: 'Bishkek, KG'
range: 'May 2021 - Dec 2022'
url: 'https://www.mancho.dev/'
---

- Actively participated in code reviews, identifying and addressing potential issues, and providing constructive feedback to team members.
- Introduced Elasticsearch for text search, resulting in a 50% reduction in search query execution time.
- Actively participated in the implementation of a continuous integration and delivery (CI/CD) pipeline, reducing deployment time by 40% and enhancing overall development efficiency.
- Designed and implemented a serverless architecture using AWS Lambda, DynamoDB, and API Gateway, resulting in improved scalability and cost efficiency.
- Mentored junior developers, providing guidance and support to enhance their technical skills and foster a collaborative team environment.
