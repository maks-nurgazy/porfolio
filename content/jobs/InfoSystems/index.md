---
date: '2022-12-01'
title: 'Java Backend Developer'
company: 'Info-Systems'
location: 'Bishkek, KG'
range: 'Dec 2022 – Jun 2023'
url: 'https://infocom.kg/'
---

- Developed and maintained robust Java backend applications using Spring Boot, Maven, and microservice architecture.
- Successfully implemented a microservice architecture for a critical application, resulting in improved scalability and fault tolerance.
- Optimized database queries, reducing query response time by 30% and enhancing overall system performance.
